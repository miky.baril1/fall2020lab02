//Mikael Baril 1844064
package lab02.eclipse;

public class Bicycle {
	//Set private objects
private String manufacturer;
private int numberGears;
private double maxSpeed;
//Getters
	public String getmanufacturer() {
		return this.manufacturer;
	}
	public int getnumberGears() {
		return this.numberGears;
	}
	public double getmaxSpeed() {
		return this.maxSpeed;
	}
	//Constructor
	public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
			this.manufacturer = manufacturer;	
			this.numberGears = numberGears;
			this.maxSpeed = maxSpeed;
		}
	public String toString() {
		return " Manufacturer: " + this.manufacturer + 
				" Number of Gears: " + this.numberGears +
				" MaxSpeed: " + this.maxSpeed;
	}
}
